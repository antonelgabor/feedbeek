import { Injectable } from "@angular/core";
import { HttpInterceptor, HttpHandler, HttpRequest, HttpEvent, HttpHeaders } from "@angular/common/http";
import { Observable, EMPTY } from 'rxjs';
import { CognitoUserPool } from 'amazon-cognito-identity-js';
import { environment } from '../../environments/environment';

const PoolData = {
   UserPoolId: environment.userPoolId,
   ClientId: environment.clientId
};

const userPool = new CognitoUserPool(PoolData);

export const withoutLoginPath = ["homepage", "login"];
const pathWithSide = ['application','dashboard']

@Injectable()
export class Interceptor implements HttpInterceptor {

   constructor() { }

   intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

      let cognitoUser: any = userPool.getCurrentUser();

      if (cognitoUser) {

         return cognitoUser.getSession(function (err, session) {

            if (err) {
               console.log("errore interceptor", err);
               alert(err);
               return;
            }

            let token;
            if (cognitoUser.hasOwnProperty('deviceKey')) {
               console.log("Occorre aggiornare devicekey");
            }

            token = session.getAccessToken().jwtToken;

            if (typeof token !== 'string') {
               console.log("Occorre aggiornare string");
            }

            if (new Date().getTime() - new Date(localStorage.getItem("refreshTime").toString()).getTime() >= 45 * 60 * 1000) {
               console.log("Aggiornamento");

               cognitoUser.refreshSession(session.getRefreshToken(), (err, session) => {
                  localStorage.setItem("refreshTime", new Date().toString())

               });
            }

            if (request.headers.get('App') == "true") {
               let appId = localStorage.getItem('appId');

               if (!appId) {
                  let path = location.pathname.split('/')[1];
                  if (!(pathWithSide.indexOf(path) > -1)) {
                     location.href = "/login"
                  }
               }
               
               const customReq = request.clone(
                  {
                     headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`,
                        'app': appId
                     })
                  }
               );
               
               return next.handle(customReq);
               
            } else {
               const customReq = request.clone(
                  {
                     headers: new HttpHeaders({
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                     })
                  }
               );
               return next.handle(customReq);
            }

         })
      } else {
         if (withoutLoginPath.indexOf(window.location.pathname.split('/')[1]) > -1) {
            return EMPTY
         } else {
            window.location.href = '/login'
         }
      }
   }


}