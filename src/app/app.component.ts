import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from '../app/services/general.service';
import { AuthenticationService } from '../app/services/authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'FeedBeek';
  showSpinner: boolean = false;
  isLogged: boolean = this.general.isLogged();
  userMail: string;
  public survTypes = [
    'Risposta aperta',
    'Voto',
    'Crocette'
  ]

  constructor(
    public general: GeneralService,
    public router: Router,
    public authService: AuthenticationService,
  ) {
    if (this.isLogged == true) {
      this.userMail = localStorage.getItem('userMail');
    }
    this.showSpinner = false
  }

  logOut() {
    this.showSpinner = true;
    this.general.logOut();
    this.isLogged = false;
    this.userMail = ''
    this.showSpinner = false;
  }
  goToPage(url = '/'): void {
    if (this.router.url != "/" + url) {
      this.showSpinner = true;
      this.router.navigateByUrl(url);
    }

  }
  logIn(url = '/', login = true): void {
    if (this.router.url != "/" + url && login == undefined) {
      this.showSpinner = true;
    }
    this.router.navigateByUrl(url + "?login=" + login)
  }
}
