import { environment } from '../../environments/environment';
import { Injectable } from '@angular/core';
import { AuthenticationDetails, CognitoUser, CognitoUserPool } from 'amazon-cognito-identity-js';


export const PoolData = {
  UserPoolId: environment.userPoolId,
  ClientId: environment.clientId,
  region: 'eu-central-1'
};
const userPool = new CognitoUserPool(PoolData);
@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  signinUser(email: string, password: string) {
    const authData = {
      Username: email,
      Password: password
    };

    const authDetails = new AuthenticationDetails(authData);
    const userData = {
      Username: email,
      Pool: userPool
    };
    const cognitoUser = new CognitoUser(userData);
    return ({ cognito: cognitoUser, details: authDetails })
  }

  /**
   * Get the userpool
   * Utilities function
   */
  getUserPool() {
    return userPool
  }

  /**
   * Logout the user
   */
  logoutUser() {
    let cognitoUser: any = userPool.getCurrentUser();
    if (cognitoUser != null) {
      userPool.getCurrentUser().signOut();
    }
    localStorage.clear()
  }
  getUserInfo() {
    let cognitoUser: any = userPool.getCurrentUser();
    if (cognitoUser) {
      return cognitoUser.getSession(function (err, session) {
        if (err) {
          this.toastr.warning('Errore!', '', {
            positionClass: 'toast-top-center'
          })
          return err;
        }
        return cognitoUser
      })
    } else {
      window.location.href = "/login"
    }
  }
}