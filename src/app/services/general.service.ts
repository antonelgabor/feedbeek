import { Router } from '@angular/router';
import { ApiService } from '../../app/services/api.service';
import { AuthenticationService } from '../../app/services/authentication.service';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { CognitoUserPool } from 'amazon-cognito-identity-js';

// Class containing functions used often in many components

const PoolData = {
  UserPoolId: environment.userPoolId,
  ClientId: environment.clientId
}

const userPool = new CognitoUserPool(PoolData);

@Injectable({
  providedIn: 'root'
})
export class GeneralService {
  constructor(
    private router: Router,
    public apiService: ApiService,
    public AuthenticationService: AuthenticationService
  ) { }

  goToPage(url = '/'): void {
    this.router.navigateByUrl(url);
  }
  logOut() {
    this.AuthenticationService.logoutUser();
    this.apiService.goToPage('homepage');
  }
  isLogged() {
    let cognitoUser: any = userPool.getCurrentUser();
    if (cognitoUser) {
      return true;
    } else {
      return false;
    }
  }
}