import { Injectable } from '@angular/core';
import { CognitoUserPool } from 'amazon-cognito-identity-js';
import { environment } from '../../environments/environment';

const PoolData = {
  UserPoolId: environment.userPoolId,
  ClientId: environment.clientId
};

const userPool = new CognitoUserPool(PoolData);
@Injectable()
export class ConfigService {

  public message: string;

  constructor() {
  }
  

  load() {
    return new Promise((resolve, reject) => {
      let cognitoUser: any = userPool.getCurrentUser();
      if (cognitoUser) {

        if (!localStorage.getItem("refreshTime") || new Date().getTime() - new Date(localStorage.getItem("refreshTime").toString()).getTime() >= 45 * 60 * 1000) {
          return cognitoUser.getSession(function (err, session) {

            let temp = session.getRefreshToken()
            return cognitoUser.refreshSession(temp, (err, session) => {
              localStorage.setItem("refreshTime", new Date().toString())
              resolve(true)
            });
          })
        } else resolve(true)
      } else resolve(true)
    });

  }

}