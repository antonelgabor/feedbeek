//
// ────────────────────────────────────────────────────────────── DEPENDECIES ────────
//
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';


// function used on CUR call, used to set the header
const appF = function (bool) {
  let headers = new HttpHeaders({
    "app": "" + bool,
    'Content-Type': 'application/json',
    'Authorization': `Bearer`,
  });
  let options = { headers: headers };
  return options
}
const deleteAppF = function (bool: boolean, obj: object) {
  let headers = new HttpHeaders({
    "app": "" + bool,
    'Content-Type': 'application/json',
    'Authorization': `Bearer`,
  });
  let options = { headers: headers, body: obj };
  return options
}

@Injectable({
  providedIn: 'root'
})

export class ApiService {
  constructor(
    private http: HttpClient,
    private router: Router
  ) { }

  // ──────────────────────────────────────────────────────────── APP ───────────────

  getApp(): Observable<any> {
    return this.http.get("https://5w9k7n8te0.execute-api.eu-central-1.amazonaws.com/test/app", appF(true));
  }
  getAllApps(): Observable<any> {
    return this.http.get<any>("https://5w9k7n8te0.execute-api.eu-central-1.amazonaws.com/test/apps", appF(false));
  }

  createApp(appName = "", appType = "") {
    return this.http.post<any>("https://5w9k7n8te0.execute-api.eu-central-1.amazonaws.com/test/app", { 'appName': appName, 'appType': appType }, appF(false));
  }

  updateApp(appName = "", appType = "",appStyle = "") {
    return this.http.put<any>("https://5w9k7n8te0.execute-api.eu-central-1.amazonaws.com/test/app", { 'appName': appName, 'appType': appType, 'appStyle': appStyle }, appF(true));
  }

  deleteApp() {
    return this.http.delete("https://5w9k7n8te0.execute-api.eu-central-1.amazonaws.com/test/app", appF(true));
  }

  // ──────────────────────────────────────────────────────────── SURVEY ───────────────

  createSurvey(survey: Survey): Observable<any> {
    return this.http.post<any>("https://k1f5e4q00c.execute-api.eu-central-1.amazonaws.com/test/survey", survey, appF(true));
  }

  updateSurvey(survey: Survey) : Observable<any>{
    return this.http.put<any>("https://k1f5e4q00c.execute-api.eu-central-1.amazonaws.com/test/survey", survey, appF(true));
  }

  deleteSurvey(id){
    return this.http.delete("https://k1f5e4q00c.execute-api.eu-central-1.amazonaws.com/test/survey", deleteAppF(true,{id:id}));
  }
  
  // ──────────────────────────────────────────────────────────── UTILITY ───────────────

  goToPage(url = '/'): void {
    this.router.navigateByUrl(url)
  }
}

export interface Survey {
  id?: string,
  title: string,
  type: string,
  text1: string,
  options?: string
}