import { BrowserModule } from '@angular/platform-browser';
import { NgModule, APP_INITIALIZER } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ConfigService } from './services/config.service';
import { ApplicationsComponent } from './pages/applications/applications.component';
import { LoginComponent } from './pages/login/login.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog'
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { Interceptor } from './interceptor/interceptor';
import { CreateEditApplicationComponent } from './pages/applications/create-edit-application/create-edit-application.component';
import { MatIconModule } from '@angular/material/icon';
import { ConfirmDeletionComponent } from './widgets/confirm-deletion/confirm-deletion.component';
import { DemoComponent } from './pages/demo/demo.component';
import { GuideComponent } from './pages/guide/guide.component';
import { SurveyComponent } from './pages/surveys/survey/survey.component';
import { SurveysComponent } from './pages/surveys/surveys.component';
import { ScrollingModule } from '@angular/cdk/scrolling';
import { MatSelectModule } from '@angular/material/select';
import { HighlightService } from './services/highlight.service';
import { PasswordResetModalComponent } from './widgets/password-reset-modal/password-reset-modal.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatStepperModule } from '@angular/material/stepper';
import { ColorPickerComponent } from './widgets/color-picker/color-picker.component';


export function initializeApp(appInitService: ConfigService) {
  return (): Promise<any> => {
    return appInitService.load();
  };
}

@NgModule({
  declarations: [
    AppComponent,
    ApplicationsComponent,
    LoginComponent,
    HomepageComponent,
    CreateEditApplicationComponent,
    ConfirmDeletionComponent,
    DemoComponent,
    GuideComponent,
    SurveyComponent,
    SurveysComponent,
    PasswordResetModalComponent,
    ColorPickerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatProgressSpinnerModule,
    MatButtonModule,
    MatInputModule,
    MatCardModule,
    MatDialogModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatIconModule,
    ScrollingModule,
    MatSelectModule,
    MatGridListModule,
    MatStepperModule
  ],
  providers: [
    ConfigService,
    HighlightService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [ConfigService],
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: Interceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
