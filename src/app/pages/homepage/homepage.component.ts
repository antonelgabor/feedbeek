import { Component, OnInit, Inject } from '@angular/core';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {

  constructor(
    @Inject(AppComponent) public parent: AppComponent
  ) { 
    this.parent.showSpinner = false;
  }

  ngOnInit(): void {
  }

}
