import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateEditApplicationComponent } from './create-edit-application.component';

describe('CreateEditApplicationComponent', () => {
  let component: CreateEditApplicationComponent;
  let fixture: ComponentFixture<CreateEditApplicationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateEditApplicationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateEditApplicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
