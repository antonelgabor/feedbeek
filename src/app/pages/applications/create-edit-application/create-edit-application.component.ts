import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ApiService } from '../../../services/api.service';
import { AppDialogData } from '../applications.component';

@Component({
  selector: 'app-create-edit-application',
  templateUrl: './create-edit-application.component.html',
  styleUrls: ['./create-edit-application.component.scss']
})
export class CreateEditApplicationComponent implements OnInit {

  app: any = this.data.app;
  creating: boolean;

  constructor(
    public apiService: ApiService,
    public snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<CreateEditApplicationComponent>,
    @Inject(MAT_DIALOG_DATA) public data: AppDialogData
  ) {
    if (this.app) {
      localStorage.setItem('appId', this.app.appId)
      this.creating = false;
    }
    else {
      this.creating = true;
      this.app = {};
    }
  }

  ngOnInit(): void {
  }

  save() {
    if (this.creating) {
      this.apiService.createApp(this.app.appName, this.app.appType).subscribe((value) => {
        // If an error occurs
        if(!(value.appId)){
          this.snackBar.open('Qualcosa è andato storto', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-failure']
          })
        }
        this.dialogRef.close(true);
      })
    }
    else {
      this.apiService.updateApp(this.app.appName, this.app.appType).subscribe((value) => {
        // If an error occurs
        if(!(value.appId)){
          this.snackBar.open('Qualcosa è andato storto', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-failure']
          })
        }
        this.dialogRef.close(true);
      })
    }
  }

}
