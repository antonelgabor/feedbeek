import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { GeneralService } from '../../services/general.service';
import { Router } from '@angular/router';
import { ApiService } from '../../services/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppComponent } from '../../app.component';
import { CreateEditApplicationComponent } from './create-edit-application/create-edit-application.component';
import { ConfirmDeletionComponent } from "../../widgets/confirm-deletion/confirm-deletion.component"

export interface AppDialogData {
  app: any;
}

@Component({
  selector: 'app-applications',
  templateUrl: './applications.component.html',
  styleUrls: ['./applications.component.scss']
})

export class ApplicationsComponent implements OnInit {


  appList: any[];
  nApp = 0;

  constructor(
    public apiService: ApiService,
    public router: Router,
    public general: GeneralService,
    public dialog: MatDialog,
    public snackBar: MatSnackBar,
    @Inject(AppComponent) public parent: AppComponent
  ) {
    this.apiService.getAllApps().subscribe((value) => {
      this.appList = value;
      this.nApp = value.length;
    })
    this.parent.showSpinner = false
  }

  ngOnInit(): void {
  }

  openApplicationDialog(appId = ""): void {
    let modalApp = this.appList.find(element => element.appId == appId)
    const dialogRef = this.dialog.open(CreateEditApplicationComponent, {
      width: '300px',
      height: '260px',
      data: { app: modalApp },
      panelClass: 'custom-dialog-container'
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result == true) {
        this.snackBar.open('Operazione completata correttamente', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-success']
        })
        this.parent.showSpinner = true;
        this.apiService.getAllApps().subscribe((value) => {
          this.appList = value;
          this.parent.showSpinner = false;
        })
      }
    });
  }

  deleteApp(appId) {
    const confirmDelete = this.dialog.open(ConfirmDeletionComponent)
    confirmDelete.afterClosed().subscribe(result => {
      if (result) {
        this.parent.showSpinner = true;
        localStorage.setItem('appId', appId)
        this.apiService.deleteApp().subscribe((value) => {
          this.apiService.getAllApps().subscribe((value) => {
            this.appList = value;
            this.nApp = value.length;
          })
          this.snackBar.open('App eliminata correttamente', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-success']
          })
        })
        this.parent.showSpinner = false;
      }
    })
  }
  goToPage(url = '/', appId = ""): void {
    this.parent.goToPage(url)
    localStorage.setItem('appId', appId);
  }
}
