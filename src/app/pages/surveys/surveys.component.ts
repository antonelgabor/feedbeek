import { Component, OnInit, Inject } from '@angular/core';
import { ApiService } from '../../services/api.service';
import { Router } from '@angular/router';
import { GeneralService } from '../../services/general.service';
import { AppComponent } from '../../app.component';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDeletionComponent } from '../../widgets/confirm-deletion/confirm-deletion.component';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-surveys',
  templateUrl: './surveys.component.html',
  styleUrls: ['./surveys.component.scss']
})
export class SurveysComponent implements OnInit {

  public app: any = history.state.app ? history.state.app : null;
  public surveys: any = history.state.surveys ? history.state.surveys : null;
  public nSurveys: number = history.state.surveys ? history.state.surveys : 0;

  constructor(
    public apiService: ApiService,
    public router: Router,
    public general: GeneralService,
    public dialog: MatDialog,
    public snackbar: MatSnackBar,
    @Inject(AppComponent) public parent: AppComponent
  ) { 
    this.apiService.getApp().subscribe((value) => {
      this.app = value;
      this.surveys = this.app.surveys;
      this.nSurveys = this.surveys.length;
    })
    this.parent.showSpinner = false;
  }

  ngOnInit(): void {}

  openSurvey(surveyId = "", chart = false): void {
    this.parent.showSpinner = true;
    let survey = this.surveys.find(element => element.id == surveyId);
    if (chart) {
      this.router.navigate(['/survey'], { state: { 'survey': survey, 'chart': true } });
    } else {
      this.router.navigate(['/survey'], { state: { 'survey': survey } });
    }
  }

  deleteSurvey(id) {
    const confirmDelete = this.dialog.open(ConfirmDeletionComponent)
    confirmDelete.afterClosed().subscribe(result => {
      if (result) {
        this.parent.showSpinner = true;
        this.apiService.deleteSurvey(id).subscribe((value) => {
          this.apiService.getApp().subscribe((value) => {
            this.app = value;
            this.surveys = this.app.surveys;
            this.nSurveys = this.surveys.length;
            this.parent.showSpinner = false;
          })
          this.snackbar.open('Questionario eliminato correttamente', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-success']
          })
        })
      }
    })
  }

  goToPage(url = "") {
    this.parent.goToPage(url)
  }
  
}
