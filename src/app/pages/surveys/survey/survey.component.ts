import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService, Survey } from '../../../services/api.service';
import { AppComponent } from '../../../app.component';
import { Chart } from '../../../../../node_modules/chart.js';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.scss']
})
export class SurveyComponent implements OnInit {

  /*survey: any = this.data.survey ? this.data.survey : {};*/
  survey: any = history.state.survey ? history.state.survey : {};
  creating: boolean = true;
  createForm: FormGroup;
  options: any = this.survey.id ? JSON.parse(this.survey.options) : ['']
  optNum = 0;
  chart: boolean = history.state.chart ? history.state.chart : false;
  // bool to check if survey has answer/answers
  answ: boolean = this.survey.answers && this.survey.answers.length > 0 ? true : false;

  constructor(
    public fb: FormBuilder,
    public apiService: ApiService,
    public router: Router,
    public snackBar: MatSnackBar,
    @Inject(AppComponent) public parent: AppComponent,
  ) {

    if (!this.chart) {
      if (this.survey.id) {
        this.creating = false
        this.createForm = fb.group({
          title: [this.survey.title, [Validators.pattern('^.{3,50}$')]],
          type: [this.survey.type, [Validators.pattern("^.{3,50}$")]],
          text1: [this.survey.text1, [Validators.pattern("^.{3,50}$")]],
          0: [this.options[0],[Validators.pattern("^.{0,50}$")]],
          1: [this.options[1],[Validators.pattern("^.{0,50}$")]],
          2: [this.options[2],[Validators.pattern("^.{0,50}$")]],
          3: [this.options[3],[Validators.pattern("^.{0,50}$")]],
          4: [this.options[4],[Validators.pattern("^.{0,50}$")]]
        })
      } else {
        this.creating = true
        this.createForm = fb.group({
          title: ['', [Validators.pattern('^.{3,50}$')]],
          type: ['', [Validators.pattern("^.{3,50}$")]],
          text1: ['', [Validators.pattern("^.{3,50}$")]],
          0: ['', [Validators.pattern("^.{0,50}$")]],
          1: ['', [Validators.pattern("^.{0,50}$")]],
          2: ['', [Validators.pattern("^.{0,50}$")]],
          3: ['', [Validators.pattern("^.{0,50}$")]],
          4: ['', [Validators.pattern("^.{0,50}$")]]
        })
      }
    }
    this.parent.showSpinner = false;
  }

  ngOnInit(): void {
  }

  get f() { return this.createForm.controls; }

  ngAfterViewInit() {
    if (this.survey.type != "Risposta aperta" && this.answ == true) {
      if (this.chart) {
        var canvas = document.getElementById("canvas") as HTMLCanvasElement;
        var ctx = canvas.getContext('2d');
        var bkColor = [
          'rgba(255, 99, 132, 0.5)',
          'rgba(54, 162, 235, 0.5)',
          'rgba(255, 206, 86, 0.5)',
          'rgba(75, 192, 192, 0.5)',
          'rgba(153, 102, 255, 0.5)',
          'rgba(255, 159, 64, 0.5)'
        ]
        var bkborderColor = [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)',
          'rgba(75, 192, 192, 1)',
          'rgba(153, 102, 255, 1)',
          'rgba(255, 159, 64, 1)'
        ]
        // N represents the number of answers
        var n = 0;
        var grades = [0, 0, 0, 0, 0]
        /* 
        "Crocette" survey structure 
        survey{ 
          answers{ 
            answer[0,1...] 
            date: "..." } 
          } 
        "Voto" survey structure 
        survey{ 
          answers{ 
            answer: integer, 
            date: "..." 
          }
        } 
        */
        if (this.survey.type == "Crocette") {
          this.survey.answers.forEach(answersArr => {
            answersArr.answer.forEach(element => {
              switch (element) {
                case
                  undefined: break;
                case 1:
                  grades[0] = grades[0] + 1;
                  n++;
                  break;
                case 2:
                  grades[1] = grades[1] + 1;
                  n++;
                  break;
                case 3:
                  grades[2] = grades[2] + 1;
                  n++;
                  break;
                case 4:
                  grades[3] = grades[3] + 1;
                  n++;
                  break;
                case 5:
                  grades[4] = grades[4] + 1;
                  n++;
                  break;
              }
            })
          });
        } else if (this.survey.type == "Voto") {
          this.survey.answers.forEach(element => {
            switch (element.answer) {
              case
                undefined: break;
              case "1":
                grades[0] = grades[0] + 1;
                n++;
                break;
              case "2":
                grades[1] = grades[1] + 1;
                n++;
                break;
              case "3":
                grades[2] = grades[2] + 1;
                n++;
                break;
              case "4":
                grades[3] = grades[3] + 1;
                n++;
                break;
              case "5":
                grades[4] = grades[4] + 1;
                n++;
                break;
            }
          });
        }

        var labels = []
        if (this.survey.type == "Voto") {
          labels = ["1", "2", "3", "4", "5"]
        } else if (this.survey.type == "Crocette") {
          labels = JSON.parse(this.survey.options)
        }
        var gradesPerc = []
        for (var i = 0; i < 5; i++) {
          gradesPerc[i] = ((grades[i] * 100) / n).toFixed(2);
        }
        var survChart = new Chart(ctx, {
          type: this.survey.type == "Crocette" ? 'pie' : 'bar',
          data: {
            labels: labels,
            datasets: [{
              label: "%",
              data: gradesPerc,
              backgroundColor: bkColor,
              borderColor: bkborderColor,
              borderWidth: 1
            }]
          },
          options: {
            legend: { 
              display: this.survey.type == "Crocette" ? true : false ,
            },
          }
        });
      }
    }
  }

  addOpt() {
    if (this.optNum < 4) {
      this.optNum++;
      this.options.push('');
    }
  }
  removeOpt(){
    if(this.optNum>=1){
      this.optNum--;
      this.options.pop()
      this.f[this.optNum+1].setValue('')
    }
  }

  submit() {
    this.parent.showSpinner = true
    try {
      // Stringify Options
      let JSONOptions = []
      for (let index = 0; index <= 5; index++) {
        if (this.f[index].value) {
          JSONOptions.push(
            this.f[index].value
          )
        } else {
          break
        }
      }
      let OPTStr = JSON.stringify(JSONOptions)

      let survey: Survey = {
        id: this.survey.id ? this.survey.id : null,
        title: this.f.title.value.trim(),
        type: this.f.type.value.trim(),
        text1: this.f.text1.value.trim(),
        options: OPTStr
      }
      if (!this.createForm.valid) {
        this.snackBar.open('Dati inseriti non validi', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-failure']
        })
        this.parent.showSpinner = false
        return
      }
      if (this.creating == true) {
        this.apiService.createSurvey(survey).subscribe((value) => {
          this.snackBar.open('Questionario creato correttamente', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-success']
          })
        })
        this.apiService.getApp().subscribe((value) => {
          let app = value;
          let surveys = app.surveys;
          let nSurveys = surveys.length;
          this.router.navigate(['/surveys'], { state: { 'surveys': surveys, 'app': app, 'nSurveys': nSurveys } });
        })

      } else {
        this.apiService.updateSurvey(survey).subscribe((value) => {
          this.snackBar.open('Questionario modificato correttamente', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-success']
          })
        })
        this.apiService.getApp().subscribe((value) => {
          let app = value;
          let surveys = app.surveys;
          let nSurveys = surveys.length;
          this.router.navigate(['/surveys'], { state: { 'app': app, 'nSurveys': nSurveys } });
        })
      }
    } catch{
      this.parent.showSpinner = false;
      this.snackBar.open('Si è verificato un errore', 'OK', {
        duration: 2000,
        verticalPosition: 'top',
        panelClass: ['snackbar-failure']
      })
    }
    this.parent.showSpinner = false
  }
  goToPage(url = "/") {
    this.parent.goToPage(url)
  }
}
