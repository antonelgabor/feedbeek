import { Component, OnInit, Inject } from '@angular/core';
import { AppComponent } from '../../app.component'
import { HighlightService } from '../../services/highlight.service';

@Component({
  selector: 'app-guide',
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss']
})
export class GuideComponent implements OnInit {

  constructor(
    public highLight: HighlightService,
    @Inject(AppComponent) public parent: AppComponent
  ) {
    this.parent.showSpinner = false;
  }

  ngOnInit(): void {
  }

  ngAfterViewInit() {
    this.highLight.highlightAll();
  }
}
