import { Component, OnInit, Inject } from '@angular/core';
import { AppComponent } from '../../app.component';
import { ApiService } from '../../services/api.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  fonts: Array<String> = [
    'Arial, sans-serif',
    'Helvetica, sans-serif',
    'Gill Sans, sans-serif',
    'Lucida, sans-serif',
    'Helvetica Narrow, sans-serif',
    'Times, serif',
    'Times New Roman, serif',
    'Palatino, serif',
    'Bookman, serif',
    'New Century Schoolbook, serif',
    'Andale Mono, monospace',
    'Courier New, monospace',
    'Courier, monospace',
    'Lucidatypewriter, monospace',
    'Fixed, monospace',
    'Comic Sans, Comic Sans MS, cursive',
    'Zapf Chancery, cursive',
    'Coronetscript, cursive',
    'Florence, cursive',
    'Parkavenue, cursive',
    'Impact, fantasy',
    'Arnoldboecklin, fantasy',
    'Oldtown, fantasy',
    'Blippo, fantasy',
    'Brushstroke, fantasy'
  ]

  appList: any;
  selectedApp: any;

  // Pop up properties
  ex_type: string = "Voto";
  ex_color: string;
  ex_size: number = 16;
  ex_font: string;
  ex_borderRadius: number = 5;
  ex_backColor: string = "FCB43A";
  ex_borderColor: string;
  ex_buttonColor: string;
  ex_buttonTextColor: string = 'eee';


  // Input properties
  ex_inputBackColor: string;
  ex_inputTextColor: string;
  ex_inputBorderRadius: number = 5;

  // Grades properties
  ex_gradeColor1: string = '992409';
  ex_gradeColor2: string = 'FFFFFF';

  // Multiple choise properties
  ex_multChColor1: string = '992409';
  ex_multChColor2: string = 'FFFFFF';

  checked0: boolean = false;
  checked1: boolean = false;
  checked2: boolean = false;
  grades: number;

  ex_answers = [
    'Può essere migliorato',
    'È semplice e pratico',
    'Ha colori piacevoli'
  ];
  ex_grades = [1, 2, 3, 4, 5]

  constructor(
    public apiService: ApiService,
    private snackBar: MatSnackBar,
    @Inject(AppComponent) public parent: AppComponent
  ) {
    this.apiService.getAllApps().subscribe((value) => {
      this.appList = value;
    })
    this.parent.showSpinner = false;
  }

  ngOnInit(): void { }

  // Check which radio button is selected to change his back-color
  compare(grade) {
    return (grade == this.grades) ? true : false;
  }
  /**
   * Function triggered to save the tailored style
   */
  public sendStyle(): void {
    localStorage.setItem('appId', this.selectedApp.appId);
    var style: any = {
      ex_color: this.ex_color,
      ex_size: this.ex_size,
      ex_font: this.ex_font,
      ex_borderRadius: this.ex_borderRadius,
      ex_backColor: this.ex_backColor,
      ex_borderColor: this.ex_borderColor,
      ex_buttonColor: this.ex_buttonColor,
      ex_buttonTextColor: this.ex_buttonTextColor,
      ex_inputBackColor: this.ex_inputBackColor,
      ex_inputTextColor: this.ex_inputTextColor,
      ex_inputBorderRadius: this.ex_inputBorderRadius,
      ex_gradeColor1: this.ex_gradeColor1,
      ex_gradeColor2: this.ex_gradeColor2,
      ex_multChColor1: this.ex_multChColor1,
      ex_multChColor2: this.ex_multChColor2
    };
    style = JSON.stringify(style)
    this.apiService.updateApp("", "", style).subscribe((value) => {
      if (value.appId) {
        this.snackBar.open('Stile salvato corretamente', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-success']
        })
      }
    })
  }
}
