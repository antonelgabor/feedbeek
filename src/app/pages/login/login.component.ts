import { Component, OnInit, Inject } from '@angular/core';
import { Observable } from 'rxjs';
import { AppComponent } from '../../app.component';
import { MatSnackBar } from '@angular/material/snack-bar';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { AuthenticationService } from '../../services/authentication.service';
import { PasswordResetModalComponent } from '../../widgets/password-reset-modal/password-reset-modal.component';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login: boolean = true;
  errorLog: any = false;

  loginForm: FormGroup;
  signUpForm: FormGroup;
  //Boolean vars to check pwd's equality
  pwd1: string;
  pwd2: string;

  constructor(
    private router: Router,
    public fb: FormBuilder,
    public dialog: MatDialog,
    public authenticationService: AuthenticationService,
    private activatedRoute: ActivatedRoute,
    public snackBar: MatSnackBar,
    @Inject(AppComponent) public parent: AppComponent
  ) {

    // URL Cleaner, removes params from URL
    activatedRoute.queryParams.subscribe((params) => {
      window.history.pushState({}, document.title, "/" + "login")
      this.login = params.login == "true" ? true : false;
    })

    // Login Form
    this.loginForm = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });

    //Sign up form
    this.signUpForm = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/)]],
      password2: ['', [Validators.required, Validators.pattern(/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/)]],
      name: ['', [Validators.required]],
      surname: ['', [Validators.required]]
    });

    this.parent.showSpinner = false
  }

  ngOnInit(): void {
    this.parent.showSpinner = false
   }

  // convenience getter for easy access to form fields
  get f() { return this.loginForm.controls; }
  get s() { return this.signUpForm.controls; }

  onSubmit() {
    this.parent.showSpinner = true;
    // The user is logging in
    if (this.login == true) {
      if (!this.loginForm.valid) {
        this.parent.showSpinner = false
        this.snackBar.open('Dati inseriti non validi', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-failure']
        })
        return
      }
      let cognitoObj = this.authenticationService.signinUser(this.f.email.value, this.f.password.value);
      cognitoObj.cognito.authenticateUser(cognitoObj.details, {
        onSuccess: (result) => {
          this.parent.showSpinner = false
          this.snackBar.open('Accesso eseguito correttamente', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-success']
          })
          this.errorLog = false;
          this.goToPage('/applications');
          localStorage.setItem("refreshTime", new Date().toString())
          localStorage.setItem("userMail", this.f.email.value)
          this.parent.userMail =  this.f.email.value;
          this.parent.isLogged = true;
        },
        onFailure: (err) => {
          this.parent.showSpinner = false
          if (err.code == "UserNotFoundException") {
            let error = "Username non esistente";
            this.snackBar.open(error, 'OK', {
              duration: 2000,
              verticalPosition: 'top',
              panelClass: ['snackbar-failure']
            })
            this.f.password.setValue("")
            this.errorLog = error;
          } else if (err.code == "NotAuthorizedException") {
            let error = "Password errata";
            this.snackBar.open(error, 'OK', {
              duration: 2000,
              verticalPosition: 'top',
              panelClass: ['snackbar-failure']
            })
            this.f.password.setValue("")
            this.errorLog = error;
          } else {
            let error = "Mail e/o password errati";
            this.snackBar.open(error, 'OK', {
              duration: 2000,
              verticalPosition: 'top',
              panelClass: ['snackbar-failure']
            })
            this.f.password.setValue("")
            this.errorLog = error;
          }
        }
      })
    }

    //The user is signing up

    else {
      if (!this.signUpForm.valid) {
        this.parent.showSpinner = false
        this.snackBar.open('Dati inseriti non validi', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-failure']
        })
        return
      }

      // Set the userPool and prepare the data that will be added

      let userPool = this.authenticationService.getUserPool();
      let attributeList = []
      var dataEmail = {
        Name: 'email',
        Value: this.s.email.value
      };
      var dataName = {
        Name: 'name',
        Value: this.s.name.value
      };
      var dataSurname = {
        Name: 'family_name',
        Value: this.s.surname.value
      };
      attributeList.push(dataEmail);
      attributeList.push(dataName);
      attributeList.push(dataSurname);

      // Make the registration as a function with observable for error handling
      function register(email, password, attributeList) {
        return Observable.create(observer => {
          userPool.signUp(email,
            password,
            attributeList,
            null,
            (err, result) => {
              if (err) {
                observer.error(err);
              }
              observer.next(result);
              observer.complete();
            });
        });
      }
      register(this.s.email.value, this.s.password.value, attributeList).subscribe(result => {

        // after the registration alert with confirm email
        this.snackBar.open('Registrazione effettuata correttamente, verifica la tua mail', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-success']
        })
        this.parent.goToPage('homepage')
      })
    }
    this.parent.showSpinner = false
  }

  goToPage(url = '/'): void {
    this.parent.goToPage(url)
  }

  /**
   * Functions that set error on 
   * control of the two passwords
   */
  openResetPwdDialog() {
    const resetPwd = this.dialog.open(PasswordResetModalComponent)
    resetPwd.afterClosed().subscribe(result => {
      if (result) {
        this.snackBar.open('La tua password è stata ripristinata', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-success']
        })
      } else {
        this.snackBar.open('Riprova più tardi', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-failure']
        })
      }
    })
  }

}
