import { Component, OnInit } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { AuthenticationService } from '../../services/authentication.service';
import { CognitoUser } from 'amazon-cognito-identity-js';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-password-reset-modal',
  templateUrl: './password-reset-modal.component.html',
  styleUrls: ['./password-reset-modal.component.scss']
})
export class PasswordResetModalComponent implements OnInit {

  // Control the stepper with boolean variables
  // Step1: get user mail
  step1Completed: boolean = false;

  email: string = "";
  verCode: string = "";
  pwd1: string = "";
  pwd2: string = "";
  userPool = this.authenticationService.getUserPool();

  constructor(
    public snackBar: MatSnackBar,
    public authenticationService: AuthenticationService,
    private dialogRef: MatDialogRef<PasswordResetModalComponent>
  ) { }

  ngOnInit(): void { }

  verifyCode() {
    // Variables initiated on purpose, otherwise the elements wouldn't be reconized inside of the confirmPassword function 
    let dialogRef = this.dialogRef;
    let snackBar = this.snackBar;
    if (this.pwd1 != this.pwd2) {
      snackBar.open('Le due password non corrispondono', 'OK', {
        duration: 2000,
        verticalPosition: 'top',
        panelClass: ['snackbar-failure']
      })
    } else {
      const userData = {
        Username: this.email,
        Pool: this.userPool
      }
      const cognitoUser = new CognitoUser(userData);
      cognitoUser.confirmPassword(this.verCode, this.pwd1, {
        onSuccess: function () {
          dialogRef.close(true)
        },
        onFailure: function (err) {
          if (err.name == "CodeMismatchException") {
            snackBar.open('Codice errato', 'OK', {
              duration: 2000,
              verticalPosition: 'top',
              panelClass: ['snackbar-failure']
            })
          } else if (err.name == "InvalidPasswordException") {
            snackBar.open('La password non rispetta i parametri', 'OK', {
              duration: 2000,
              verticalPosition: 'top',
              panelClass: ['snackbar-failure']
            })
          } else {
            dialogRef.close(false)
          }
        }
      })
    }
  }

  sendResetCode(stepper) {
    const userData = {
      Username: this.email,
      Pool: this.userPool
    }
    const cognitoUser = new CognitoUser(userData);
    cognitoUser.forgotPassword({
      onSuccess: () => {
        this.snackBar.open('Verifica la tua casella postale', 'OK', {
          duration: 2000,
          verticalPosition: 'top',
          panelClass: ['snackbar-success']
        })
        stepper.next()
      },
      onFailure: (err) => {
        if (err.name == "UserNotFoundException") {
          this.snackBar.open('Non esiste un account associato a questa e-mail', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-failure']
          })
        }
        else if (err.name == "InvalidParameterException") {
          this.snackBar.open('Devi inserire una e-mail', 'OK', {
            duration: 2000,
            verticalPosition: 'top',
            panelClass: ['snackbar-failure']
          })
        } else {
          this.dialogRef.close(false)
        }
      },
    })
  }

}
