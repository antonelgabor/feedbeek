import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ApplicationsComponent } from './pages/applications/applications.component';
import { LoginComponent } from './pages/login/login.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { SurveysComponent } from './pages/surveys/surveys.component';
import { SurveyComponent } from './pages/surveys/survey/survey.component';
import { GuideComponent } from './pages/guide/guide.component';
import { DemoComponent } from './pages/demo/demo.component';


const routes: Routes = [
  { path: '', redirectTo: 'homepage', pathMatch: 'full' },
  { path: 'homepage', component: HomepageComponent },
  { path: 'login', component: LoginComponent },
  { path: 'applications', component: ApplicationsComponent },
  { path: 'surveys', component: SurveysComponent },
  { path: 'survey', component: SurveyComponent },
  { path: 'guide', component: GuideComponent },
  { path: 'demo', component: DemoComponent },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
